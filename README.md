### INSTALLATION

## 1) Install dependencies

`npm install`

## 2) Install program globally

```
npm install -g .
```

### usage

`dict def <word> : Prints the definition of the word`

`dict syn <word> : Prints synonym for the word`

`dict ant <word> : Prints antonym for the word`

`dict ex <word> : Prints examples for the word`

`dict play : Play word game`

`dict help : Prints all the commands available`

`dict : Prints word of the day with complete details`

##### 3) Uninstall

```
npm uninstall -g dictionary-cli
```

## 4) Run program without installing globally

### usage

`./index.js def <word> : Prints the definition of the word`

`./index.js syn <word> : Prints synonym for the word`

`./index.js ant <word> : Prints antonym for the word`

`./index.js ex <word> : Prints examples for the word`

`./index.js play : Play word game`

`./index.js help : Prints all the commands available`

`./index.js : Prints word of the day with complete details`
