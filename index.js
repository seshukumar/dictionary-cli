#!/usr/bin/node

// parse arguments
let [flag, word] = process.argv.splice(2);

/* 
      if word is not provided  and flag is not 'help' and play assign flag to word and set flag to 'dict'
      to display full dictionary 
*/
if ((flag && flag !== 'help' && flag !== 'play') && !word) {
      word = flag;
      flag = 'dict';
} else if (!flag) {
      flag = 'word-of-the-day';
}

const { handlePromiseAndDisplayResult } = require('./libs/display.js');

switch (flag) {

      case 'def': // print definition of the word
            const { getDefinition } = require('./libs/definition');
            handlePromiseAndDisplayResult(getDefinition, word);
            break;

      case 'syn': // print synonym
            const { getSynonym } = require('./libs/synonym');
            handlePromiseAndDisplayResult(getSynonym, word)
            break;

      case 'ant': // print antonym
            const { getAntonym } = require('./libs/antonym');
            handlePromiseAndDisplayResult(getAntonym, word);
            break;

      case 'ex': // print examples
            const { getExamples } = require('./libs/examples');
            handlePromiseAndDisplayResult(getExamples, word);
            break;

      case 'dict': // print complete details of the word
            const { getAllDetails } = require('./libs/dictionary');
            handlePromiseAndDisplayResult(getAllDetails, word);
            break;

      case 'word-of-the-day': // prints word of the day
            const { getWordOfTheDay } = require('./libs/word-of-the-day');
            getWordOfTheDay();
            break;

      case 'play': // play word game
            const { game } = require('./libs/game');
            game();
            break;

      case 'help':
      default: // print default value
            console.log(`
                  def <word>        : Prints the definition of the word
                  syn <word>        : Prints synonym for the word
                  ant <word>        : Prints antonym for the word
                  ex <word>         : Prints examples for the word
                  dict              : Prints complete details of the word
                  play              : Play word game
            `);
            break;

}
