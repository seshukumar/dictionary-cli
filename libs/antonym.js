const { get } = require('./http');
const { HOST } = require('../config');

module.exports.getAntonym = async (word) => {
      try {
            const relatedWords = await get(`${HOST}word/${word}/relatedWords`);
            let antonyms = [];
            // extract antonym from the related words
            if (relatedWords && relatedWords.length > 0) {
                  relatedWords.forEach((relatedWord) => {
                        if (relatedWord.relationshipType == "antonym") {
                              antonyms.push(...relatedWord.words);
                        }
                  });
            }

            return {
                  head: ['Word', 'Antonym'],
                  data: antonyms
            };
      } catch (error) {
            throw new Error(error.message);
      }

}