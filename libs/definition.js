const { get } = require('./http');
const { HOST } = require('../config');

module.exports.getDefinition = async (word) => {
      try {
            let result = await get(`${HOST}word/${word}/definitions`);
            let definitions = result.map(definition => definition.text);
            
            return {
                  head: ['Word', 'Definition'],
                  data: definitions
            };
      } catch (error) {
            throw new Error(error.message);
      }
}
