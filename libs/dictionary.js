const { getDefinition } = require('./definition');
const { getExamples } = require('./examples');
const { getAntonym } = require('./antonym');
const { getSynonym } = require('./synonym');

module.exports.getAllDetails = async (word) => {
            const libs = [getAntonym, getSynonym, getDefinition, getExamples];
            try {
                  const promises = libs.map(lib => lib(word));
                  return await Promise.all(promises);
            } catch(error) {
                  throw new Error(error.message);
            }
}