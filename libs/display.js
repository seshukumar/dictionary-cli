const Table = require('cli-table2');

function displayResult(result, word) {
      const { head, data } = result;

      if(!head || !data) {
            console.log("Invalid configuration");
            return;
      }

      let table = new Table({ head , colWidths: [10, 150],   style: {head: ['green']}});

      if (Array.isArray(data)) {
            table.push([word, data.join('\n')]);
            console.log(table.toString());
      } else {
            table.push([word, data]);
            console.log(table.toString());
      }

}

module.exports.handlePromiseAndDisplayResult = (promise, word) => {
      promise(word).then(result => {
            if(Array.isArray(result)) {
                  result.forEach(data => {
                        displayResult(data, word);
                  });
            } else {
                  displayResult(result, word)
            }
      })
      .catch(error => {
            console.log(error.message);
      });
}

module.exports.displayResult = displayResult;