const { get } = require('./http');
const { HOST } = require('../config');

module.exports.getExamples = async (word) => {
      try {
            let { examples } = await get(`${HOST}word/${word}/examples`);
            return {
                  head: ['Word', 'Examples'],
                  data: examples.map(example => example.text)
            }
      } catch (error) {
            throw new Error(error.message);
      };
}