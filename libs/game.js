
const { getDefinition } = require('./definition');
const { getAntonym } = require('./antonym');
const { getSynonym } = require('./synonym');
const { getAllDetails } = require("./dictionary");
const { handlePromiseAndDisplayResult } = require("./display");
const { get } = require('./http');
const { HOST } = require('../config');

const readline = require('readline');
const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
});

let correctAnswer;
let question;
let results = [];
module.exports.game = async () => {

      let { word } = await get(`${HOST}words/randomWord`);

      correctAnswer = word;

      let options = [
            { name: 'definition', function: getDefinition },
            { name: 'antonym', function: getAntonym },
            { name: 'synonym', function: getSynonym }
      ];

      try {

            let randomOption;

            /*
                  loop until we have results to form the question 
            */
            while (results.length === 0) {
                  let randomNumber = getRandomNumber(options.length);
                  randomOption = options[randomNumber];
                  let { data } = await randomOption.function(word);
                  results = data;
            }

            question = `What is the correct word for ${randomOption.name} "${getRandomItem()}"?\nAnswer:\t`;
            askQuestion(question, handleAnswer);

      } catch (error) {
            console.log("Error", error.message);
      }
}

function askQuestion(question, handleAnswer) {
      rl.question(question, (answer) => {
            handleAnswer(answer);
      });
}


function handleAnswer(answer) {
      if (answer === correctAnswer) {
            console.log("Your answer is correct. ");
            rl.close();
      } else {
            handleWrongAnswer();
      }
}

function handleWrongAnswer() {

      let options = 'wrong answer:\n you can try these options \n 1  try again \n 2  hint \n 3  quit \n Answer: \t';

      // display options
      askQuestion(options, (answer) => {
            switch (parseInt(answer)) {

                  case 2: // hint
                        let hint;

                        if (results && results.length > 1) {
                              hint = getRandomItem();
                        } else {
                              hint = results[0].reverse();
                        }

                        askQuestion(`Hint: ${hint} \n Answer: \t`, handleAnswer);
                        break;

                  case 3: // quit
                        console.log("Answer: ", correctAnswer);
                        handlePromiseAndDisplayResult(getAllDetails, correctAnswer);
                        rl.close();
                        break;

                  case 1:
                  default:
                        askQuestion(question, handleAnswer);
                        break;

            }
      });
}

//  return a random item from the result
function getRandomItem() {
      let randomNumber = getRandomNumber(results.length);
      return results[randomNumber];
}

function getRandomNumber(maxNumber) {
      return Math.floor(Math.random() * maxNumber)
}