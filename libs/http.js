const axios = require("axios").default;
const { API_KEY } = require('../config');

module.exports.get = async (url) => {
      url = `${url}?api_key=${API_KEY}`;
      try {
            const result = await axios.get(url);
            return result.data;
      } catch (error) {
            throw new Error(error.response.data.error);
      }
};
