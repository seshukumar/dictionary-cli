const { get } = require('./http');
const { HOST } = require('../config');
module.exports.getSynonym = async (word) => {

      let synonyms = [];

      try {
            const relatedWords = await get(`${HOST}word/${word}/relatedWords`);
            // extract synonym from the related words
            if (relatedWords && relatedWords.length > 0) {
                  relatedWords.forEach((relatedWord) => {
                        if (relatedWord.relationshipType == "synonym") {
                              synonyms.push(...relatedWord.words);
                        }
                  });
            }

            return {
                  head: ['Word', 'Synonym'],
                  data: synonyms
            };
      } catch (error) {
            throw new Error(error.message);
      }
}