const { getAllDetails } = require('./dictionary');
const { handlePromiseAndDisplayResult } = require('./display');
const { get } = require('./http');
const { HOST } = require('../config');

const fs = require('fs');

let wordOfTheDay;
let endTime;

module.exports.getWordOfTheDay = async () => {

      let currentTime = Date.now();
      let data = fs.readFileSync(__dirname + '/word-of-the-day.json');

      if (data.toString()) {
            data = JSON.parse(data);
            if (data.endTime) {
                  endTime = data.endTime;
                  wordOfTheDay = data.wordOfTheDay;
            }
      }

      if (!endTime || currentTime > endTime) {
           await setData();
      }

      try {
            console.log("word of the day: ", wordOfTheDay);
            handlePromiseAndDisplayResult(getAllDetails, wordOfTheDay);
      } catch (error) {
            throw new Error(error.message);
      }

      
}

async function setData() {
      // if no data exists in the json
      endTime = setNewEndTime();

      let result = await get(`${HOST}words/randomWord`);
      
      wordOfTheDay = result.word;
      let data = {
            endTime,
            wordOfTheDay
      }

      fs.writeFileSync(__dirname + '/word-of-the-day.json', JSON.stringify(data));

}


function setNewEndTime() {
      let date = new Date();
      date.setHours(23, 59, 59, 999);
      return Date.parse(date);
}